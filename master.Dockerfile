FROM debian:latest

RUN apt-get update && apt-get install -y vim python net-tools telnet curl sshpass ansible ssh
